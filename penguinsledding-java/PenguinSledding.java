public class PenguinSledding {

	public long countDesigns(int numCheckpoints, int[] checkpoint1,
			int[] checkpoint2) {
		boolean[][] edges = new boolean[numCheckpoints][numCheckpoints];
		for (int i = 0; i < checkpoint1.length; i++) {
			edges[checkpoint1[i] - 1][checkpoint2[i] - 1] = true;
			edges[checkpoint2[i] - 1][checkpoint1[i] - 1] = true;
		}
		int[] degree = new int[numCheckpoints];
		for(int i = 0; i < checkpoint1.length; i++) {
			degree[checkpoint1[i] - 1]++;
			degree[checkpoint2[i] - 1]++;
		}
		long stars = 0;
		for(int i = 0; i < numCheckpoints; i++) {
			stars += Math.pow(2, degree[i]) - 1 - degree[i];
		}
		
		long triangles = 0;
		for(int i = 0; i < numCheckpoints; i++) {
			for(int j = i + 1; j < numCheckpoints; j++) {
				for(int k = j + 1; k < numCheckpoints; k++) {
					if(edges[i][j] && edges[j][k] && edges[i][k]) {
						triangles++;
					}
				}
			}
		}
		return 1 + stars + checkpoint1.length + triangles;
	}

}
