import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PenguinEmperorTest {

    protected PenguinEmperor solution;

    @Before
    public void setUp() {
        solution = new PenguinEmperor();
    }

    @Test
    public void testCase0() {
        int numCities = 3;
        long daysPassed = 2L;

        int expected = 2;
        int actual = solution.countJourneys(numCities, daysPassed);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        int numCities = 4;
        long daysPassed = 3L;

        int expected = 2;
        int actual = solution.countJourneys(numCities, daysPassed);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        int numCities = 5;
        long daysPassed = 36L;

        int expected = 107374182;
        int actual = solution.countJourneys(numCities, daysPassed);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        int numCities = 300;
        long daysPassed = 751L;

        int expected = 413521250;
        int actual = solution.countJourneys(numCities, daysPassed);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        int numCities = 300;
        long daysPassed = 750L;

        int expected = 0;
        int actual = solution.countJourneys(numCities, daysPassed);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase5() {
        int numCities = 350;
        long daysPassed = 1000000000000000000L;

        int expected = 667009739;
        int actual = solution.countJourneys(numCities, daysPassed);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase6() {
        int numCities = 5;
        long daysPassed = 7L;

        int expected = 12;
        int actual = solution.countJourneys(numCities, daysPassed);

        Assert.assertEquals(expected, actual);
    }

}
