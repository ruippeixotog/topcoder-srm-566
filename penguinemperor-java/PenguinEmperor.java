public class PenguinEmperor {

	int MOD = 1000000007;

	public int countJourneys(int numCities, long daysPassed) {
		int[][] cyclePos = new int[numCities][numCities];
		cyclePos[0][0] = 1;
		for (int d = 1; d < numCities; d++) {
			for (int i = 0; i < numCities; i++) {
				int prev = (i + d) % numCities;
				int next = (i - d + numCities) % numCities;

				cyclePos[d][prev] = modAdd(cyclePos[d][prev],
						cyclePos[d - 1][i]);

				if (prev != next) {
					cyclePos[d][next] = modAdd(cyclePos[d][next],
							cyclePos[d - 1][i]);
				}
			}
			if (daysPassed == d) {
				return cyclePos[d][0];
			}
		}
		
		int[] cycle = cyclePos[numCities - 1];
		cycle = combineExp(cycle, daysPassed / numCities);
		int remDays = (int) (daysPassed % numCities);
		if (remDays != 0) {
			cycle = combine(cycle, cyclePos[remDays]);
		}
		return cycle[0];
	}

	int modAdd(int a, int b) {
		return (int) ((a + (long) b) % MOD);
	}

	int modMult(int a, int b) {
		return (int) ((a * (long) b) % MOD);
	}

	int[] combineExp(int[] ma, long pow) {
		if (pow == 1)
			return ma;
		int[] mr = combineExp(combine(ma, ma), pow / 2);
		if (pow % 2 == 1) {
			return combine(mr, ma);
		}
		return mr;
	}

	int[] combine(int[] ma, int[] mb) {
		int[] mr = new int[ma.length];
		for (int i = 0; i < mr.length; i++) {
			for (int j = 0; j < mr.length; j++) {
				mr[i] = modAdd(mr[i],
						modMult(ma[j], mb[(j - i + mb.length) % mb.length]));
			}
		}
		return mr;
	}
}
